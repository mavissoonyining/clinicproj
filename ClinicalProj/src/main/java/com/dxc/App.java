package com.dxc;

import java.util.ArrayList;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.dxc.entity.Clinic;
import com.dxc.entity.Patient;


public class App {
	private static SessionFactory factory;

	public static void main(String[] args) {
		try {
			factory = new Configuration().configure("resource/hibernate.cfg.xml").buildSessionFactory();

		} catch (Throwable ex) {
			System.err.println("Failed to create sessionFactory object " + ex);
			throw new ExceptionInInitializerError(ex);
		}

		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();

		Clinic c1 = new Clinic();
		c1.setComponentName("Height");
		c1.setComponentValue(1.72);
		c1.setMeasuredDateTime(new Date());

		Clinic c2 = new Clinic();
		c2.setComponentName("Weight");
		c2.setComponentValue(65.9);
		c2.setMeasuredDateTime(new Date());

		ArrayList<Clinic> list1 = new ArrayList<Clinic>();
		list1.add(c1);
		list1.add(c2);

		Patient p1 = new Patient();
		p1.setFirstName("Amy");
		p1.setLastName("Tan");
		p1.setAge(12);
		p1.setClinicalData(list1);

		Clinic c3 = new Clinic();
		c3.setComponentName("Height");
		c3.setComponentValue(1.77);
		c3.setMeasuredDateTime(new Date());

		Clinic c4 = new Clinic();
		c4.setComponentName("Weight");
		c4.setComponentValue(59.8);
		c4.setMeasuredDateTime(new Date());

		ArrayList<Clinic> list2 = new ArrayList<Clinic>();
		list2.add(c3);
		list2.add(c4);

		Patient p2 = new Patient();
		p2.setFirstName("Bob");
		p2.setLastName("Lee");
		p2.setAge(25);
		p2.setClinicalData(list2);

		session.persist(p1);
		session.persist(p2);
		tx.commit();

		session.close();
		System.out.println("success");

	}
}
