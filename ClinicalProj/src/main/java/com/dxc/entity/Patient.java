package com.dxc.entity;

import java.util.List;

import javax.persistence.*;



@Entity
@Table(name = "patient")
public class Patient {
	
	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String firstName;
	private String lastName;
	private int age;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="patient id")
	@OrderColumn(name="type")
	private List<Clinic> clinicalData;
	
	public Patient() {
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	public List<Clinic> getClinicalData() {
		return clinicalData;
	}

	public void setClinicalData(List<Clinic> clinicalData) {
		this.clinicalData = clinicalData;
	}
}
