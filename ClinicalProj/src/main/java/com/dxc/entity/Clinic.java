package com.dxc.entity;

import javax.persistence.*;


import java.util.Date;

@Entity
@Table(name = "clinic")
public class Clinic {
	
	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String componentName;
	private double componentValue;
	private Date measuredDateTime;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public double getComponentValue() {
		return componentValue;
	}
	public void setComponentValue(double componentValue) {
		this.componentValue = componentValue;
	}
	public Date getMeasuredDateTime() {
		return measuredDateTime;
	}
	public void setMeasuredDateTime(Date measuredDateTime) {
		this.measuredDateTime = measuredDateTime;
	}
	
}
